import csv = require('csv-parser');
import * as fs from 'fs';
import { Request, Response } from 'express';
import * as sqlite3 from 'sqlite3';

const dbFile = 'cosechas.sqlite';
const db = new sqlite3.Database(dbFile);

class CsvController {
  static async cargarCsv(req: Request, res: Response) {
    try {
        const filePath = req.file?.path; // Obtener la ruta del archivo CSV subido
        if (!filePath) {
          return res.status(400).json({ error: 'Archivo CSV no encontrado' });
        }
      const results: any[] = [];

      fs.createReadStream(filePath)
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', () => {
          // Procesar los datos del CSV y guardar en la base de datos
          results.forEach((row) => {
            db.run(`INSERT INTO Cosechas (
              mail_agricultor,
              nombre_agricultor,
              apellido_agricultor,
              mail_cliente,
              nombre_cliente,
              apellido_cliente,
              nombre_campo,
              ubicacion_campo,
              fruta_cosechada,
              variedad_cosechada
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
            [
              row.mail_agricultor,
              row.nombre_agricultor,
              row.apellido_agricultor,
              row.mail_cliente,
              row.nombre_cliente,
              row.apellido_cliente,
              row.nombre_campo,
              row.ubicacion_campo,
              row.fruta_cosechada,
              row.variedad_cosechada,
            ]);
          });

          res.status(200).json({ message: 'Datos del CSV cargados correctamente' });
        });
    } catch (error) {
      res.status(500).json({ error: 'Error al procesar el archivo CSV' });
    }
  }
}

export default CsvController;
