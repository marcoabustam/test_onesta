import * as sqlite3 from 'sqlite3';

// Clase que representa el servicio de validación
class ValidacionService {
  private db: sqlite3.Database;

  constructor(db: sqlite3.Database) {
    this.db = db;
  }

  // Método para validar correo electrónico único para agricultores
  validarCorreoAgricultor(correo: string, callback: (error: Error | null, existe: boolean) => void) {
    this.db.get(`SELECT id FROM Cosechas WHERE mail_agricultor = ?`, [correo], (err, agricultorRow) => {
      if (err) {
        callback(err, false);
      } else {
        callback(null, !!agricultorRow);
      }
    });
  }

  // Método para validar correo electrónico único para clientes
  validarCorreoCliente(correo: string, callback: (error: Error | null, existe: boolean) => void) {
    this.db.get(`SELECT id FROM Cosechas WHERE mail_cliente = ?`, [correo], (err, clienteRow) => {
      if (err) {
        callback(err, false);
      } else {
        callback(null, !!clienteRow);
      }
    });
  }

  // Agregar más métodos de validación según sea necesario
}

export default ValidacionService;
