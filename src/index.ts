import express, { Request, Response } from 'express';
import * as sqlite3 from 'sqlite3';
import ValidacionService from './services/validation-service';
import multer from 'multer';
import CsvController from './controllers/csv-controller';


const app = express();
const port = 3000;

// Configuración de la base de datos SQLite
const dbFile = 'cosechas.sqlite';
const db = new sqlite3.Database(dbFile);
const validacionService = new ValidacionService(db);

// Configurar multer para subida de archivos CSV
const storage = multer.diskStorage({
    destination: function (_req, _file, cb) {
      cb(null, 'uploads/'); // Directorio donde se guardarán los archivos subidos
    },
    filename: function (_req, file, cb) {
      cb(null, file.originalname); // Nombre original del archivo
    },
  });
  
  const upload = multer({ storage: storage });

app.use(express.json());

// Crear tabla de frutas
db.serialize(() => {
    db.run(`CREATE TABLE IF NOT EXISTS Cosechas (
    id INTEGER PRIMARY KEY,
    mail_agricultor TEXT UNIQUE,
    nombre_agricultor TEXT,
    apellido_agricultor TEXT,
    mail_cliente TEXT UNIQUE,
    nombre_cliente TEXT,
    apellido_cliente TEXT,
    nombre_campo TEXT,
    ubicacion_campo TEXT,
    fruta_cosechada TEXT,
    variedad_cosechada TEXT
  )`);
});

// API post para agregar una cosecha
app.post('/api/cosechas', (req: Request, res: Response) => {
    const {
        mail_agricultor,
        nombre_agricultor,
        apellido_agricultor,
        mail_cliente,
        nombre_cliente,
        apellido_cliente,
        nombre_campo,
        ubicacion_campo,
        fruta_cosechada,
        variedad_cosechada,
    } = req.body;

    //A pesar del manejo en el identificador UNIQUE en la DB, validamos para responder con un Json la Validación de correo electrónico único para agricultores. Si no manejamos 
    // esta excepción, no me devolverá un error si el correo ya existe en la base de datos.
    validacionService.validarCorreoAgricultor(mail_agricultor, (err, existeAgricultor) => {
        if (err) {
            console.error(err.message);
            return res.status(500).json({ error: 'Error al verificar el correo electrónico del agricultor' });
        }

        if (existeAgricultor) {
            return res.status(400).json({ error: 'El correo electrónico del agricultor ya está registrado' });
        }
        validacionService.validarCorreoCliente(mail_cliente, (err, existeCliente) => {
            if (err) {
                console.error(err.message);
                return res.status(500).json({ error: 'Error al verificar el correo electrónico del cliente' });
            }

            if (existeCliente) {
                return res.status(400).json({ error: 'El correo electrónico del cliente ya está registrado' });
            }

            // Validación de combinación única de nombre y ubicación de campos
            db.get(`SELECT id FROM Cosechas WHERE nombre_campo = ? AND ubicacion_campo = ?`, [nombre_campo, ubicacion_campo], (err, campoRow) => {
                if (err) {
                    console.error(err.message);
                    return res.status(500).json({ error: 'Error al verificar la combinación de nombre y ubicación del campo' });
                }

                if (campoRow) {
                    return res.status(400).json({ error: 'La combinación de nombre y ubicación del campo ya está registrada' });
                }

                // Validación de nombre único de fruta
                db.get(`SELECT id FROM Cosechas WHERE fruta_cosechada = ?`, [fruta_cosechada], (err, frutaRow) => {
                    if (err) {
                        console.error(err.message);
                        return res.status(500).json({ error: 'Error al verificar el nombre de la fruta' });
                    }

                    if (frutaRow) {
                        return res.status(400).json({ error: 'El nombre de la fruta ya está registrado' });
                    }
                    // Validación de combinación única de fruta y variedad
                    db.get(`SELECT id FROM Cosechas WHERE fruta_cosechada = ? AND variedad_cosechada = ?`, [fruta_cosechada, variedad_cosechada], (err, variedadRow) => {
                        if (err) {
                            console.error(err.message);
                            return res.status(500).json({ error: 'Error al verificar la combinación de fruta y variedad' });
                        }

                        if (variedadRow) {
                            return res.status(400).json({ error: 'La combinación de fruta y variedad ya está registrada' });
                        }
                        // Insertar la cosecha en la base de datos si todas las validaciones pasan
                        db.run(`INSERT INTO Cosechas ( 
                                    mail_agricultor,
                                    nombre_agricultor,
                                    apellido_agricultor,
                                    mail_cliente,
                                    nombre_cliente,
                                    apellido_cliente,
                                    nombre_campo,
                                    ubicacion_campo,
                                    fruta_cosechada,
                                    variedad_cosechada
                                ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
                            [
                                mail_agricultor,
                                nombre_agricultor,
                                apellido_agricultor,
                                mail_cliente,
                                nombre_cliente,
                                apellido_cliente,
                                nombre_campo,
                                ubicacion_campo,
                                fruta_cosechada,
                                variedad_cosechada,
                            ],
                            (err) => {
                                if (err) {
                                    console.error(err.message);
                                    return res.status(500).json({ error: 'Error al insertar la cosecha en la base de datos' });
                                }
                                return res.status(201).json({ message: 'Cosecha agregada correctamente' });
                            });
                        return;
                    });
                    return; // esta parte del codigo se ve horrible (los returns), lo sé, pero es la manera mas sencilla de manejar 
                });         // err y los rows de la base de datos sin que me genere una advertencia de código TS7006 se pueden omitir, transpilará de igual manera los archivos js.
                return;
            });
            return;
        });
        return;
    });
});

// API para cargar CSV
app.post('/api/cargar-csv', upload.single('csvFile'), CsvController.cargarCsv);

// API get para obtener todas las cosechas
app.get('/api/cosechas', (_req: Request, res: Response) => {
    db.all('SELECT * FROM Cosechas', (err, rows) => {
        if (err) {
            console.error(err.message);
            return res.status(500).json({ error: 'Error al consultar las cosechas en la base de datos' });
        }

        return res.status(200).json(rows);
    });
});

// Inicializar servidor
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
