"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Clase que representa el servicio de validación
class ValidacionService {
    constructor(db) {
        this.db = db;
    }
    // Método para validar correo electrónico único para agricultores
    validarCorreoAgricultor(correo, callback) {
        this.db.get(`SELECT id FROM Cosechas WHERE mail_agricultor = ?`, [correo], (err, agricultorRow) => {
            if (err) {
                callback(err, false);
            }
            else {
                callback(null, !!agricultorRow);
            }
        });
    }
    // Método para validar correo electrónico único para clientes
    validarCorreoCliente(correo, callback) {
        this.db.get(`SELECT id FROM Cosechas WHERE mail_cliente = ?`, [correo], (err, clienteRow) => {
            if (err) {
                callback(err, false);
            }
            else {
                callback(null, !!clienteRow);
            }
        });
    }
}
exports.default = ValidacionService;
