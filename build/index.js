"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const sqlite3 = __importStar(require("sqlite3"));
const validation_service_1 = __importDefault(require("./services/validation-service"));
const multer_1 = __importDefault(require("multer"));
const csv_controller_1 = __importDefault(require("./controllers/csv-controller"));
const app = (0, express_1.default)();
const port = 3000;
// Configuración de la base de datos SQLite
const dbFile = 'cosechas.sqlite';
const db = new sqlite3.Database(dbFile);
const validacionService = new validation_service_1.default(db);
// Configurar multer para subida de archivos CSV
const storage = multer_1.default.diskStorage({
    destination: function (_req, _file, cb) {
        cb(null, 'uploads/'); // Directorio donde se guardarán los archivos subidos
    },
    filename: function (_req, file, cb) {
        cb(null, file.originalname); // Nombre original del archivo
    },
});
const upload = (0, multer_1.default)({ storage: storage });
app.use(express_1.default.json());
// Crear tabla de frutas
db.serialize(() => {
    db.run(`CREATE TABLE IF NOT EXISTS Cosechas (
    id INTEGER PRIMARY KEY,
    mail_agricultor TEXT UNIQUE,
    nombre_agricultor TEXT,
    apellido_agricultor TEXT,
    mail_cliente TEXT UNIQUE,
    nombre_cliente TEXT,
    apellido_cliente TEXT,
    nombre_campo TEXT,
    ubicacion_campo TEXT,
    fruta_cosechada TEXT,
    variedad_cosechada TEXT
  )`);
});
// API post para agregar una cosecha
app.post('/api/cosechas', (req, res) => {
    const { mail_agricultor, nombre_agricultor, apellido_agricultor, mail_cliente, nombre_cliente, apellido_cliente, nombre_campo, ubicacion_campo, fruta_cosechada, variedad_cosechada, } = req.body;
    //A pesar del manejo en el identificador UNIQUE en la DB, validamos para responder con un Json la Validación de correo electrónico único para agricultores. Si no manejamos 
    // esta excepción, no me devolverá un error si el correo ya existe en la base de datos.
    validacionService.validarCorreoAgricultor(mail_agricultor, (err, existeAgricultor) => {
        if (err) {
            console.error(err.message);
            return res.status(500).json({ error: 'Error al verificar el correo electrónico del agricultor' });
        }
        if (existeAgricultor) {
            return res.status(400).json({ error: 'El correo electrónico del agricultor ya está registrado' });
        }
        validacionService.validarCorreoCliente(mail_cliente, (err, existeCliente) => {
            if (err) {
                console.error(err.message);
                return res.status(500).json({ error: 'Error al verificar el correo electrónico del cliente' });
            }
            if (existeCliente) {
                return res.status(400).json({ error: 'El correo electrónico del cliente ya está registrado' });
            }
            // Validación de combinación única de nombre y ubicación de campos
            db.get(`SELECT id FROM Cosechas WHERE nombre_campo = ? AND ubicacion_campo = ?`, [nombre_campo, ubicacion_campo], (err, campoRow) => {
                if (err) {
                    console.error(err.message);
                    return res.status(500).json({ error: 'Error al verificar la combinación de nombre y ubicación del campo' });
                }
                if (campoRow) {
                    return res.status(400).json({ error: 'La combinación de nombre y ubicación del campo ya está registrada' });
                }
                // Validación de nombre único de fruta
                db.get(`SELECT id FROM Cosechas WHERE fruta_cosechada = ?`, [fruta_cosechada], (err, frutaRow) => {
                    if (err) {
                        console.error(err.message);
                        return res.status(500).json({ error: 'Error al verificar el nombre de la fruta' });
                    }
                    if (frutaRow) {
                        return res.status(400).json({ error: 'El nombre de la fruta ya está registrado' });
                    }
                    // Validación de combinación única de fruta y variedad
                    db.get(`SELECT id FROM Cosechas WHERE fruta_cosechada = ? AND variedad_cosechada = ?`, [fruta_cosechada, variedad_cosechada], (err, variedadRow) => {
                        if (err) {
                            console.error(err.message);
                            return res.status(500).json({ error: 'Error al verificar la combinación de fruta y variedad' });
                        }
                        if (variedadRow) {
                            return res.status(400).json({ error: 'La combinación de fruta y variedad ya está registrada' });
                        }
                        // Insertar la cosecha en la base de datos si todas las validaciones pasan
                        db.run(`INSERT INTO Cosechas ( 
                                    mail_agricultor,
                                    nombre_agricultor,
                                    apellido_agricultor,
                                    mail_cliente,
                                    nombre_cliente,
                                    apellido_cliente,
                                    nombre_campo,
                                    ubicacion_campo,
                                    fruta_cosechada,
                                    variedad_cosechada
                                ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, [
                            mail_agricultor,
                            nombre_agricultor,
                            apellido_agricultor,
                            mail_cliente,
                            nombre_cliente,
                            apellido_cliente,
                            nombre_campo,
                            ubicacion_campo,
                            fruta_cosechada,
                            variedad_cosechada,
                        ], (err) => {
                            if (err) {
                                console.error(err.message);
                                return res.status(500).json({ error: 'Error al insertar la cosecha en la base de datos' });
                            }
                            return res.status(201).json({ message: 'Cosecha agregada correctamente' });
                        });
                        return;
                    });
                    return; // esta parte del codigo se ve horrible (los returns), lo sé, pero es la manera mas sencilla de manejar 
                }); // err y los rows de la base de datos sin que me genere una advertencia de código TS7006 se pueden omitir, transpilará de igual manera los archivos js.
                return;
            });
            return;
        });
        return;
    });
});
// API para cargar CSV
app.post('/api/cargar-csv', upload.single('csvFile'), csv_controller_1.default.cargarCsv);
// API get para obtener todas las cosechas
app.get('/api/cosechas', (_req, res) => {
    db.all('SELECT * FROM Cosechas', (err, rows) => {
        if (err) {
            console.error(err.message);
            return res.status(500).json({ error: 'Error al consultar las cosechas en la base de datos' });
        }
        return res.status(200).json(rows);
    });
});
// Inicializar servidor
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
