"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const csv = require("csv-parser");
const fs = __importStar(require("fs"));
const sqlite3 = __importStar(require("sqlite3"));
const dbFile = 'cosechas.sqlite';
const db = new sqlite3.Database(dbFile);
class CsvController {
    static cargarCsv(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var _a;
            try {
                const filePath = (_a = req.file) === null || _a === void 0 ? void 0 : _a.path; // Obtener la ruta del archivo CSV subido
                if (!filePath) {
                    return res.status(400).json({ error: 'Archivo CSV no encontrado' });
                }
                const results = [];
                fs.createReadStream(filePath)
                    .pipe(csv())
                    .on('data', (data) => results.push(data))
                    .on('end', () => {
                    // Procesar los datos del CSV y guardar en la base de datos
                    results.forEach((row) => {
                        db.run(`INSERT INTO Cosechas (
              mail_agricultor,
              nombre_agricultor,
              apellido_agricultor,
              mail_cliente,
              nombre_cliente,
              apellido_cliente,
              nombre_campo,
              ubicacion_campo,
              fruta_cosechada,
              variedad_cosechada
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, [
                            row.mail_agricultor,
                            row.nombre_agricultor,
                            row.apellido_agricultor,
                            row.mail_cliente,
                            row.nombre_cliente,
                            row.apellido_cliente,
                            row.nombre_campo,
                            row.ubicacion_campo,
                            row.fruta_cosechada,
                            row.variedad_cosechada,
                        ]);
                    });
                    res.status(200).json({ message: 'Datos del CSV cargados correctamente' });
                });
            }
            catch (error) {
                res.status(500).json({ error: 'Error al procesar el archivo CSV' });
            }
        });
    }
}
exports.default = CsvController;
